<?php

declare(strict_types=1);

namespace App\API;

use App\Post\Application\Command\GetThePostCommand;
use App\Shared\Domain\Bus\CommandBus;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

#[Route(path: '/posts/{post_id}', methods: ['GET'])]
class GetThePostController
{
    public function __construct(
        private readonly CommandBus $commandBus
    ) {
    }

    public function __invoke($post_id): JsonResponse
    {
        $command = new GetThePostCommand(
            id: $post_id,
        );

        try {
            $this->commandBus->dispatch(
                command: $command,
            );
        } catch (Exception $exception) {
            return new JsonResponse(
                [
                    'error' => $exception->getMessage(),
                ],
                Response::HTTP_BAD_REQUEST,
            );
        }
        print_r($command);die;
        // return new JsonResponse(
        //     [
        //         'post_id' => $command->id,
        //         'post_title' => $command->title,
        //         'post_summary' => $command->summary,
        //         'post_description' => $command->description,
        //     ],
        //     Response::HTTP_OK,
        // );
    }
}

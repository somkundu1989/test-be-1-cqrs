<?php

declare(strict_types=1);

namespace App\Post\Application\Command;

use App\Shared\Domain\Bus\Command;

class GetThePostCommand implements Command
{
    public function __construct(
        public readonly ?string $id,
    ) {
    }
}

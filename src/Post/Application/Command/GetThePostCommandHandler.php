<?php

declare(strict_types=1);

namespace App\Post\Application\Command;

use App\Post\Domain\Post;
use App\Post\Domain\PostRepository;
use App\Shared\Domain\Bus\CommandHandler;
use Symfony\Component\Uid\Uuid;

class GetThePostCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly PostRepository $repository
    ) {
    }

    public function __invoke(GetThePostCommand $command): Post
    {
        $post_id = $command->id;
        $thePost = $this->repository->find((string)$post_id);
        print_r($thePost);die;
    }
}
